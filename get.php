 <?php
	echo "\n";
	$n 	= (int)readline("Numarul de necunoscute: ");
	$m	= (int)readline("Numarul   de   ecuatii: ");



	$coef	= array();

	for($i = 1; $i <= $m; $i++)
	{
			$temp 		= readline("Ecuatia " . $i . ": ");
			$coef[$i]	=	explode(" ", $temp);
	}
	$temp	= readline("Z(X): ");
	$z		= explode(" ", $temp);

	// getTab($coef);

	// SCHIMBAREA SEMNULUI DACA ESTE NU CORESPUNDE
	foreach ($coef as $rowKey => $row)
	{
		if ($row[$n] == ">=")
		{
			foreach ($row as $colKey => $cell)
			{
				if ($cell != ">=")
				{
					$coef[$rowKey][$colKey] *= -1;
				}
				else
				{
					$coef[$rowKey][$colKey] = "<=";
				}
			}
		}
	}

	getTab($coef);

	// ADAUGAREA VARIABILELOR AUXILIARE
	foreach ($coef as $rowKey => $row)
	{
		$coef[$rowKey]["SIGN"] 	= $coef[$rowKey][$n];
		$coef[$rowKey]["RES"] 	= $coef[$rowKey][$n + 1];
		for($i = $n; $i < $n + $m; $i++)
		{
		if ($i == $n - 1 + $rowKey)
			{
				$coef[$rowKey][$i] = 1;
			}
			else
			{
				$coef[$rowKey][$i] = 0;
			}
		}
	}

	echo "\n";
	getTab($coef);
	echo "\n";

	// FORMAREA TABELULUI SIMPLEX
	foreach ($coef as $rowKey => $row)
	{
		$ind	= $n + $rowKey;
		$simp[$ind]["BAZA"]	= $ind;
		$simp[$ind]["Ci"]		= 0;
		$simp[$ind]["B"]		= $row["RES"];

		for($i = 0; $i < $n + $m; $i++)
		{
			$simp[$ind][$i + 1] = $row[$i];
		}
	}
	$simp["RES"]["RES"] = 0;
	for($i = 0; $i < $n + $m; $i++)
	{
		if ($i < $n)
		{
			$simp["RES"][$i + 1] = (int)$z[$i] * (-1);
		}
		else
		{
			$simp["RES"][$i + 1] = 0;
		}
	}

	echo "\n";
	getTab($simp);
	echo "\n";



	function getTab($tab)
	{
		foreach ($tab as $rowKey => $row)
		{
			foreach ($row as $colKey => $cell)
			{
				echo "[" . $rowKey . "][" . $colKey . "] : " . $cell . " ";
			}
			echo "\n";
		}
	}

?>
