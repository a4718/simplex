<?php

    $type   = true;
    $html   = "";
    $log    = "SIMPLEX : ";

    if ($type) {
        $rows       = $_GET["rows"];
        $cols       = $_GET["cols"];
        $z          = normVector(explode(" ", $_GET["eq"]));

        $coefStr    = $_GET["simplex"];

        foreach (preg_split("/((\r?\n)|(\r\n?))/", $coefStr) as $line) {
            $coef[] = normVector(explode(" ", $line));
        }
    } else {
        $rows   = 4;
        $cols   = 8;

        $coef[] = normVector(explode(" ", "1 0 1 0 1 0 0 0 = 146"));
        $coef[] = normVector(explode(" ", "0 1 0 1 0 1 0 0 = 130"));
        $coef[] = normVector(explode(" ", "42 36 0 0 0 0 -1 0 = 4600"));
        $coef[] = normVector(explode(" ", "0 0 52 46 0 0 0 -1 = 7860"));
        $z      = normVector(explode(" ", "10920 9360 5720 5060"));
    }
    // $coef[] = normVector(explode(" ", "1 0 1 0 <= 146"));
    // $coef[] = normVector(explode(" ", "0 1 0 1 <= 130"));
    // $coef[] = normVector(explode(" ", "42 36 0 0 >= 4600"));
    // $coef[] = normVector(explode(" ", "0 0 52 46 >= 7860"));
    // $z      = normVector(explode(" ", "10920 9360 5720 5060"));

    // $coef[] = normVector(explode(" ", "  1   0   1   0   <=  146"));
    // $coef[] = normVector(explode(" ", "  0   1   0   1   <=  130"));
    // $coef[] = normVector(explode(" ", "  42  36  0   0   >=  4600"));
    // $coef[] = normVector(explode(" ", "  0   0   52  46  >=  7860"));
    // $z      = normVector(explode(" ", "  10920   9360    5720    5060"));

    // $coef[] = normVector(explode(" ", "1 0 -42 0 -1 0 0 0 = 10920"));
    // $coef[] = normVector(explode(" ", "0 1 -36 0 0 -1 0 0 = 9360"));
    // $coef[] = normVector(explode(" ", "1 0 0 -52 0 0 1 0 = 5720"));
    // $coef[] = normVector(explode(" ", "0 1 0 -46 0 0 0 1 = 5060"));
    // $z      = normVector(explode(" ", "146 130 4600 7860"));


    // SCHIMBAREA SEMNULUI DACA NU CORESPUNDE
    $coef   = normSystem($coef, $n);

    // ADAUGAREA VARIABILELOR AUXILIARE
    $temp = addVars($coef, $cols, $rows);
    $coef = $temp["tab"];
    $cols = $temp["n"];
    $rows = $temp["m"];

    unset($temp);

    // ---------------------------------------------
    $coef   = normVector($coef);

    // FORMAREA TABELULUI SIMPLEX
    $simp = getSimplex($coef, $z, $cols, $rows);

    // -----------------------------------------------
    if (!$type) {
        $piv["line"]    = 1;
        $piv["col"]     = 3;
        $html           = "<br>" . getHtmlTab($simp, $z, $cols, $rows, $piv) . "<br>";
        $simp           = nextStep($simp, $z, $cols, $rows, $piv);

        $piv["line"]    = 1;
        $piv["col"]     = 1;
        $html           .= "<br>" . getHtmlTab($simp, $z, $cols, $rows, $piv) . "<br>";
        $simp           = nextStep($simp, $z, $cols, $rows, $piv);

        $piv["line"]    = 2;
        $piv["col"]     = 4;
        $html           .= "<br>" . getHtmlTab($simp, $z, $cols, $rows, $piv) . "<br>";
        $simp           = nextStep($simp, $z, $cols, $rows, $piv);

        $piv["line"]    = 2;
        $piv["col"]     = 2;
        $html           .= "<br>" . getHtmlTab($simp, $z, $cols, $rows, $piv) . "<br>";
        $simp           = nextStep($simp, $z, $cols, $rows, $piv);

        $piv["line"]    = 3;
        $piv["col"]     = 7;
        $html           .= "<br>" . getHtmlTab($simp, $z, $cols, $rows, $piv) . "<br>";
        $simp           = nextStep($simp, $z, $cols, $rows, $piv);

        $html           .= "<br>" . getHtmlTab($simp, $z, $cols, $rows) . "<br>";
    } else {
        while (!checkStop($simp, $cols, $rows)) {
            $piv    = getPivot($simp, $cols, $rows);
            $html   .= "<br>" . getHtmlTab($simp, $z, $cols, $rows, $piv) . "<br>";
            $simp   = nextStep($simp, $z, $cols, $rows, $piv);
        }

        $html   .= "<br> " . getHtmlTab($simp, $z, $cols, $rows) . "<br>";
    }

    $file   = fopen("/output.html", "w");
    fwrite($file, $html . getHtmlResult($simp, $z, $cols, $rows));

    echo $html . getHtmlResult($simp, $z, $cols, $rows);

    // --------------------------------------------------------------------------------------------------
    // FUNCTIONS

    // DONE
    function z_x($ci, $B)
    {
        $newB = array();
        $it   = 1;
        $res  = 0;

        foreach ($ci as $key => $value) {
            $res = $res + ($value * $B[$key]);
        }

        return $res;
    }

    // DONE
    function getVecFromCol($tab, $col)
    {
        $res = array();
        $it = 1;

        foreach ($tab as $rowKey => $row) {
            $res[$it++]  = $row[$col];
        }

        return $res;
    }

    // DONE
    function normVector($tab)
    {
        $new = array();
        $it = 1;
        
        foreach ($tab as $key => $value) {
            $new[$it++] = $value;
        }

        return $new;
    }

    // DONE
    function normSystem($tab, $n)
    {
        foreach ($tab as $rowKey => $row) {
            if ($row[$n] == ">=") {
                foreach ($row as $colKey => $cell) {
                    if ($cell != ">=") {
                        $tab[$rowKey][$colKey] *= -1;
                    } else {
                        $tab[$rowKey][$colKey] = "<=";
                    }
                }
            }
        }

        return $tab;
    }

    // DONE
    function addVars($tab, $n, $m)
    {
        $newN = $n;

        foreach ($tab as $rowKey => $row) {
            $tab[$rowKey]["SIGN"] = $tab[$rowKey][$n + 1];
            $tab[$rowKey]["RES"] = $tab[$rowKey][$n + 2];
            unset($tab[$rowKey][$n + 1]);
            unset($tab[$rowKey][$n + 2]);

            if ($tab[$rowKey]["SIGN"] != "=") {
                $newN = $n + $m;
                
                for ($i = $n + 1; $i <= $newN; $i++) {
                    if ($i == $n + $rowKey + 1) {
                        $tab[$rowKey][$i] = 1;
                    } else {
                        $tab[$rowKey][$i] = 0;
                    }
                }
            }
        }

        $res["tab"] = $tab;
        $res["n"] = $newN;
        $res["m"] = $m;
        return $res;
    }

    // DONE
    function getBase($tab, $n, $m)
    {
        $res = array();

        for ($i = 1; $i <= $n; $i++) {
            $oneCount = 0;
            $other    = false;
            $oneCol   = -1;
            $oneLine  = -1;

            for ($j = 1; $j <= $m; $j++) {
                if (abs($tab[$j][$i]) == 1) {
                    $oneCount++;
                    $oneLine  = $j;
                    $oneCol   = $i;
                } else if ((int)$tab[$j][$i] != 0) {
                    $other = true;
                }
            }
            
            if (!$other && $oneCount == 1) {
                $res[$oneLine]  = $oneCol;
            }
        }

        return $res;
    }
    
    // DONE
    function getSimplex($coef, $z, $n, $m)
    {
        $base = getBase($coef, $n, $m);

        foreach ($coef as $rowKey => $row) {
            $line               = $rowKey;
            $simp[$line][0]     = $row["RES"];
            $simp[$line]["Bs"]  = $base[$rowKey];
            
            if (isset($z[$base[$rowKey]])) {
                $simp[$line]["Ci"]  = $z[$base[$rowKey]];
            } else {
                $simp[$line]["Ci"]  = 0;
            }

            for ($i = 1; $i <= $n; $i++) {
                $simp[$line][$i] = $row[$i];
            }
        }

        $last               = $m + 1;
        $simp[$last][0]     = z_x(getVecFromCol($simp, "Ci"), getVecFromCol($simp, 0));
        $simp[$last]["Ci"]  = "/";
        $simp[$last]["Bs"]  = "/";

        for ($i = 1; $i <= $n; $i++) {
            $sum = 0;
            
            for ($j = 1; $j <= $m; $j++) {
                $sum = $sum + ($simp[$j][$i] * $simp[$j]["Ci"]);
            }

            if (isset($z[$i])) {
                $sum = $sum - $z[$i];
            }

            $simp[$last][$i] = $sum;
        }

        return $simp;
    }

    // DONE
    function nextStep($simp, $z, $n, $m, $pivot)
    {
        if (!isset($pivot)) {
            $pivot  = getPivot($simp, $n, $m);
        }

        if (isset($pivot["line"])) {
            $simp[$pivot["line"]]["Bs"] = $pivot["col"];
        } else {
            return $simp;
        }

        if (isset($z[$pivot["col"]])) {
            // echo " DEBUG : " . "Z[" . $pivot["col"] . "] : " . $z[$pivot["col"]] . "\n";
            $simp[$pivot["line"]]["Ci"]  = $z[$pivot["col"]];
        } else {
            $simp[$pivot["line"]]["Ci"]  = 0;
        }

        $start["line"]    = 1;
        $start["col"]     = 0;
        $simp             = jordan($simp, $start, $n, $m, $pivot);

        return $simp;
    }

    function getPivot($tab, $n, $m)
    {
        $min   = PHP_INT_MAX;
        $res   = array();

        for($col = 1; $col <= $n; $col++)
        {
            if ($tab[$m + 1][$col] < 0)
            {
                for($line = 1; $line <= $m; $line++)
                {
                    if ($tab[$line][$col] > 0)
                    {
                        if ($min > ($tab[$line][0] / $tab[$line][$col]))
                        {
                            $min          = $tab[$line][0] / $tab[$line][$col];
                            $res["line"]  = $line;
                            $res["col"]   = $col;
                        }
                    }
                }
                if (isset($res["line"]))
                {
                    break;
                }
            }
        }
        return $res;
    }

    // DONE
    function jordan($tab, $start, $n, $m, $pivot)
    {
        $res = $tab;
        $m++;

        for ($line = $start["line"]; $line <= $m; $line++) {
            if ($line != $pivot["line"]) {
                $res[$line][$pivot["col"]] = 0;
            }
        }

        for ($col = $start["col"]; $col <= $n; $col++) {
            $res[$pivot["line"]][$col] = $tab[$pivot["line"]][$col] / $tab[$pivot["line"]][$pivot["col"]];
        }

        for ($line = $start["line"]; $line <= $m; $line++) {
            for ($col = $start["col"]; $col <= $n; $col++) {
                if ($line != $pivot["line"] && $col != $pivot["col"]) {
                    $res[$line][$col] = (($tab[$line][$col] * $tab[$pivot["line"]][$pivot["col"]]) - ($tab[$line][$pivot["col"]] * $tab[$pivot["line"]][$col])) / $tab[$pivot["line"]][$pivot["col"]];
                }
            }
        }

        return $res;
    }

    function checkStop($tab, $n, $m)
    {
        for ($i = 1; $i <= $n; $i++) {
            $count = 0;
            
            for ($j = 1; $j <= $m + 1; $j++) {
                if ((int)$tab[$j][$i] < 0) {
                    $count++;
                }
            }
            
            if ($count == $m + 1) {
                return 1;
            }
        }

        if (is_null(getPivot($tab, $n, $m))) {
            return 3;
        }

        for ($i = 1; $i <= $n; $i++) {
            if ($tab[$m + 1][$i] < 0) {
                return 0;
            }
        }

        return 2;
    }

    function getHtmlTab($tab, $z, $n, $m, $pivot = null)
    {
        $showPivot = false;

        if (isset($pivot["line"]) && isset($pivot["col"])) {
            $showPivot = true;
        }
        $res  = "   <style>
                        hr {
                            border-collapse: collapse;
                            border-top: 1px solid black;
                            border-bottom: 0px solid transparent;
                            padding: 0;
                            margin: 0;
                            width: 100%;
                        }
                        table, td, th {
                            border: 1px solid black;
                        }
                        table {
                            border-collapse: collapse;
                            width: 100%;
                        }
                        th {
                            height: 35px;
                        }
                    </style>";

        $body = "";
        $row  = "";
        $head = "   <th>Baza</th>
                    <th>C<sub>i</sub></th>
                    <th>B</th>";
        foreach ($tab[1] as $colKey => $value) {
            if (!isset($z[$colKey])) {
                $z[$colKey]   = 0;
            }

            if ($colKey > 0) {
                $head  .= " <td  align='center'>
                                    " . $z[$colKey] . "
                                    <br><hr>
                                    x<sub>" . $colKey . "</sub>
                            </td>";
            }
        }

        $head = "<tr>" . $head . "</tr>\n";

        for($line = 1; $line <= $m + 1; $line++) {
            if ($line == $m + 1) {
                $row  = "<td align='center' colspan = '2'>&Delta;<sub>j</sub> = Z<sub>j</sub> - C<sub>j</sub></td>\n";
            } else {
                $row  = "   <td  align='center'>x<sub>" . $tab[$line]["Bs"] . "</sub></td>
                            <td align='center'>" . $tab[$line]["Ci"] . "</td>";
            }

            for ($col = 0; $col <= $n; $col++) {
                if ($showPivot) {
                    if ($line == $pivot["line"] && $col == $pivot["col"]) {
                        $row .= "   <td align='center'>
                                        <table style='width:35%; height:20%;'>
                                            <td align='center'>" . round($tab[$line][$col], 2) . "</td>
                                        </table>
                                    </td>";
                    } else {
                        $row .= "<td align='center'>" . round($tab[$line][$col], 2) . "</td>\n";
                    }
                } else {
                    $row .= "<td align='center'>" . round($tab[$line][$col], 2) . "</td>\n";
                }
            }
            $body .= "<tr>" . $row . "</tr>\n";
        }

        $res .= "<table>" . $head . $body . "</table>";

        return $res;
    }

    function getHtmlResult($tab, $z, $n, $m)
    {
        $body = "<br><hr>";
        $body .= "<p>RASPUNS : <br>";

        $res  = checkStop($tab, $n, $m);
        if ($res == 1) {
            $body .= "<h4>NU ARE SOLUTIE!</h4><br>";
        } else {
            $body .= "X<sup>*</sup> = (";

            for ($i = 1; $i <= $n; $i++) {
                $stop = false;
                
                for ($j = 1; $j <= $m; $j++) {
                    if ($i == $tab[$j]["Bs"]) {
                        $body .= " " . round($tab[$j][0], 2) . "";
                        $stop  = true;
                    }
                }

                if (!$stop) {
                    $body .= " 0";
                }

                if ($i != $n) {
                    $body .= ", ";
                } else {
                    $body .= ")<br>";
                }
            }

            $body .= "Z(X<sup>*</sup>) = " . round($tab[$m + 1][0], 2) . "</p><br>";
        }

        return $body;
    }
