 <?php

    $result  = array();
	$n       = 2;
	$m	     = 2;

    // $coef[] = normVector(explode(" ", "1 0 1 0 1 0 0 0 = 146"));
    // $coef[] = normVector(explode(" ", "0 1 0 1 0 1 0 0 = 130"));
    // $coef[] = normVector(explode(" ", "42 36 0 0 0 0 -1 0 = 4600"));
    // $coef[] = normVector(explode(" ", "0 0 52 46 0 0 0 -1 = 7860"));
    // $z      = normVector(explode(" ", "10920 9360 5720 5060"));

    $coef[] = normVector(explode(" ", "1 2 >= 0"));
    $coef[] = normVector(explode(" ", "3 4 <= 5"));
    $z      = normVector(explode(" ", "10 20"));

    // SCHIMBAREA SEMNULUI DACA NU CORESPUNDE
    $coef   = normSystem($coef, $n, $m);
	echo "SYSTEM : \n" . getTab($coef) . "\n";

	// ADAUGAREA VARIABILELOR AUXILIARE
    $temp   = addVars($coef, $n, $m);
    $coef   = $temp["tab"];
    $n      = $temp["n"];
    $m      = $temp["m"];

    echo "SYSTEM : \n" . getTab($coef) . "\n";
    // ---------------------------------------------

    $coef   = normVector($coef);

    echo "SYSTEM : \n" . getTab($coef) . "\n";

	// echo "\n" . getTable($coef) . "\n";

    // FORMAREA TABELULUI SIMPLEX
    $simp = getSimplex($coef, $z, $n, $m);
    echo "TABEL SIMPLEX FORMAT : \n";
    echo "\n" . getTab($simp) . "\n";



    // -----------------------------------------------------
  // ECHO A BIDIMENSIONAL TABLE
  function getTab($table)
  {
      $sizeInfo   = array();
      $sizeTotal  = 0;
      $colCount   = 0;
      $keyMax     = 0;
      $wall       = "|";
      $res        = "";
      $header     = "";
      $first     = true;

      foreach ($table as $rowKey => $row)
      {
          if ($keyMax < strlen($rowKey))
          {
              $keyMax = strlen($rowKey);
          }
          foreach($row as $colKey => $col)
          {
              if (isset($sizeInfo[$colKey]))
              {
                  if ($sizeInfo[$colKey] < strlen($col))
                  {
                      $sizeInfo[$colKey] = strlen($col);
                  }
              }
              else
              {
                  $sizeInfo[$colKey] = strlen($col);
              }
          }
      }

      foreach ($sizeInfo as $key => $value) {
          $sizeTotal  += (int)$value;
          $colCount++;
      }

      $res    .= " " . fill($sizeTotal + $colCount * (strlen($wall) + 2) + 2 * strlen($wall) + $keyMax + 2, "-") . "\n";
      $header = $res;
      foreach ($table as $rowKey => $row)
      {
          if($first)$header .= " " . $wall . " " . "/" . fill($keyMax - strlen("/"), " ");
          $res    .= " " . $wall . " " . $rowKey . fill($keyMax - strlen($rowKey), " ");
          foreach ($row as $colKey => $col)
          {
              if($first)$header .= " " . $wall . " " . $colKey . fill($sizeInfo[$colKey] - strlen($colKey), " ");
              $res    .= " " . $wall . " " . $col . fill($sizeInfo[$colKey] - strlen($col), " ");
          }
          if($first)$header .= " " . $wall . "\n";
          $first = false;
          $res    .= " " . $wall . "\n";
          $res    .= " " . fill($sizeTotal + $colCount * (strlen($wall) + 2) + 2 * strlen($wall) + $keyMax + 2, "-") . "\n";
      }
      return $header . $res;
  }

  // GET STRING WITH LENGHT = [$value] AND CONTAINS [$value] CHARACTERS OF [$chr]
  function fill($value, $chr)
  {
      $res    = '';
      for($it = 0; $it < $value; $it++)
      {
          $res .= $chr;
      }
      return $res;
  }

// --------------------------------------------------------------------------------------------------
  function z_x($ci, $B)
  {
    $newB = array();
    $it   = 1;
    $res  = 0;

    foreach ($ci as $key => $value)
    {
      $res = $res + ($value * $B[$key]);
    }
    return $res;
  }
  // DONE
  function getVecFromCol($tab, $col)
  {
    $res  = array();
    $it   = 1;

    foreach ($tab as $rowKey => $row)
    {
      $res[$it++]  = $row[$col];
    }
    return $res;
  }
  // DONE
  function normVector($tab)
  {
    $new  = array();
    $it   = 1;
    foreach ($tab as $key => $value)
    {
      $new[$it++] = $value;
    }
    return $new;
  }
  // DONE
  function normSystem($tab, $n, $m)
  {
    foreach ($tab as $rowKey => $row)
  	{
  		if ($row[$n + 1] == ">=")
  		{
  			foreach ($row as $colKey => $cell)
  			{
  				if ($cell != ">=")
  				{
  					$tab[$rowKey][$colKey] *= -1;
  				}
                else
                {
                    $tab[$rowKey][$colKey]  = "<=";
                }
  			}
  		}
  	}
    return $tab;
  }

  function addVars($tab, $n, $m)
  {
      foreach ($tab as $rowKey => $row)
      {
          $tab[$rowKey]["SIGN"] 	= $tab[$rowKey][$n + 1];
          $tab[$rowKey]["RES"] 	  = $tab[$rowKey][$n + 2];
          unset($tab[$rowKey][$n + 1]);
          unset($tab[$rowKey][$n + 2]);

          if ($tab[$rowKey]["SIGN"] != "=")
          {
            $newN = $n + $m;
            for($i = $n + 1; $i <= $newN; $i++)
            {
              if ($i == $n + $rowKey + 1)
              {
                  $tab[$rowKey][$i] = 1;
              }
              else
              {
                $tab[$rowKey][$i] = 0;
              }
            }
          }
        }

        $res["tab"] = $tab;
        $res["n"]   = $newN;
        $res["m"]   = $m;
        return $res;
    }

  function getBase($tab, $n, $m)
  {
    $res = array();
    for($i = 1; $i <= $n; $i++)
    {
      $oneCount = 0;
      $other    = false;
      $oneCol   = -1;
      $oneLine  = -1;
      for($j = 1; $j <= $m; $j++)
      {
        if (abs($tab[$j][$i]) == 1)
        {
          $oneCount++;
          $oneLine  = $j;
          $oneCol   = $i;
        }
        else if ((int)$tab[$j][$i] != 0) $other = true;
      }
      if (!$other && $oneCount == 1)
      {
        $res[$oneLine]  = $oneCol;
      }
    }
    return $res;
  }

  function getSimplex($coef, $z, $n, $m)
  {
    $base   = getBase($coef, $n, $m);
    foreach ($coef as $rowKey => $row)
  	{
  		$line               = $rowKey;
      $simp[$line][0]       = $row["RES"];
      $simp[$line]["Bs"]    = $base[$rowKey];
      if (isset($z[$base[$rowKey]]))
      {
  		    $simp[$line]["Ci"]  = $z[$base[$rowKey]];
      }
      else {
          $simp[$line]["Ci"]  = 0;
      }

  		for($i = 1; $i <= $n; $i++)
  		{
  			$simp[$line][$i] = $row[$i];
  		}
  	}
    $last = $m + 1;

    $simp[$last][0]     = z_x(getVecFromCol($simp, "Ci"), getVecFromCol($simp, 0));
    $simp[$last]["Ci"]  = "/";
    $simp[$last]["Bs"]  = "/";

    for($i = 1; $i <= $n; $i++)
  	{
      $sum = 0;
      for($j = 1; $j <= $m; $j++)
      {
        $sum = $sum + ($simp[$j][$i] * $simp[$j]["Ci"]);
      }

      if (isset($z[$i]))
      {
        $sum = $sum - $z[$i];
      }

      $simp[$last][$i] = $sum;
  	}
    return $simp;
  }

  function nextStep($simp, $z, $n, $m, $pivot = null)
  {
      if (!isset($pivot))
      {
          $pivot  = getPivot($simp, $n, $m);
      }

      if (isset($pivot["line"]))
      {
          $simp[$pivot["line"]]["Bs"] = $pivot["col"];
      }
      else
      {
          return $simp;
      }

      if (isset($z[$pivot["col"]]))
      {
          $simp[$pivot["line"]]["Ci"]  = $z[$pivot["col"]];
      }
      else {
          $simp[$pivot["line"]]["Ci"]  = 0;
      }

      $start["line"]    = 1;
      $start["col"]     = 0;
      $simp             = jordan($simp, $start, $n, $m, $pivot);

      return $simp;
  }

  function getPivot($tab, $n, $m)
  {
      $min  = PHP_INT_MAX;

      for($col = 1; $col <= $n; $col++)
      {
          if ($tab[$m + 1][$col] < 0)
          {
              for($line = 1; $line <= $m; $line++)
              {
                  if ($tab[$line][$col] > 0)
                  {
                      if ($min > ($tab[$line][0] / $tab[$line][$col]))
                      {
                          $min          = $tab[$line][0] / $tab[$line][$col];
                          $res["line"]  = $line;
                          $res["col"]   = $col;
                      }
                  }
              }
              if (isset($res["line"]))
              {
                  break;
              }
          }
      }
    return $res;
}

  function jordan($tab, $start, $n, $m, $pivot)
  {
    $res  = $tab;
    $m++;
    for($line = $start["line"]; $line <= $m; $line++)
    {
      if ($line != $pivot["line"])
      {
        $res[$line][$pivot["col"]]  = 0;
      }
    }

    for($col = $start["col"]; $col <= $n; $col++)
    {
        $res[$pivot["line"]][$col]  = $tab[$pivot["line"]][$col] / $tab[$pivot["line"]][$pivot["col"]];
    }

    for($line = $start["line"]; $line <= $m; $line++)
    {
      for($col = $start["col"]; $col <= $n; $col++)
      {
        if ($line != $pivot["line"] && $col != $pivot["col"])
        {
          $res[$line][$col] =   (($tab[$line][$col] * $tab[$pivot["line"]][$pivot["col"]]) - ($tab[$line][$pivot["col"]] * $tab[$pivot["line"]][$col]))
                                / $tab[$pivot["line"]][$pivot["col"]];
        }
      }
    }
    return $res;
  }

  function checkStop($tab, $n, $m)
  {
    for($i = 1; $i <= $n; $i++)
    {
      $count = 0;
      for($j = 1; $j <= $m + 1; $j++)
      {
        if ((int)$tab[$j][$i] < 0)
        {
          $count++;
        }
      }
      if ($count == $m + 1)
      {
        return 1;
      }
    }

    if (is_null(getPivot($tab, $n, $m)))
    {
      return 3;
    }

    for($i = 1; $i <= $n; $i++)
    {
      if ($tab[$m + 1][$i] < 0)
      {
        return 0;
      }
    }

    return 2;
  }
